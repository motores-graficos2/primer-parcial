using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlayer : MonoBehaviour
{
    private float RapidezDesplazamiento = 10.0f;
    public Camera CameraFPS;
    public GameObject Bullet;
    public TMPro.TMP_Text TextLife;
    public TMPro.TMP_Text TextKills;
    public TMPro.TMP_Text TextGameOver;
    public TMPro.TMP_Text TextRestart;
    public TMPro.TMP_Text TextTimer;
    private int Life;
//    private int Kills;
    private float Seconds;
    public LayerMask Floor;
    public float JumpMagnitude;
    public CapsuleCollider col;
    private Rigidbody rb;
    private int JumpTimes;
    private int Jumps;
    private bool IsOnFloor = true;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        Cursor.lockState = CursorLockMode.Locked;
        Starting();
    }

    // Update is called once per frame
    void Update()
    {
        float MovimientoVertical = Input.GetAxis("Vertical") * RapidezDesplazamiento;
        float MovimientoHorizontal = Input.GetAxis("Horizontal") * RapidezDesplazamiento;

        MovimientoHorizontal *= Time.deltaTime;
        MovimientoVertical *= Time.deltaTime;

        transform.Translate(MovimientoHorizontal, 0, MovimientoVertical);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = CameraFPS.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(Bullet, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(CameraFPS.transform.forward * 50, ForceMode.Impulse);

            Destroy(pro, 2);
        }

        if (Input.GetKeyDown("r"))
        {
            Starting();
        }

        if (Input.GetKeyDown("space") && (IsOnFloor || Jumps < JumpTimes))
        {
            rb.AddForce(Vector3.up * JumpMagnitude, ForceMode.Impulse);
            IsOnFloor = false;
            Jumps++;
        }
    }

    private void Starting()
    {
        Jumps = 0;
        JumpTimes = 2;
        Life = 3;
//        Kills = 0;
        Seconds = 60;
        TextGameOver.text = "";
        TextRestart.text = "";
        Texts();
        StartCoroutine(Timer(60));
    }

    private void Texts()
    {
        TextKills.text = "Destroy all cubes";
        TextLife.text = "HP = " + Life;
        if (Life == 0)
        {
            GameOver();
        }
    }
 
    private void GameOver()
    {
        TextGameOver.text = " GAME OVER ";
        TextRestart.text = " Press R to restart ";
        TextLife.text = "";
        TextTimer.text = "";
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            Hurt();
        }
        if (collision.gameObject.CompareTag("Piso"))
        {
            Jumps = 0;
            IsOnFloor = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp") == true)
        {
            RapidezDesplazamiento = 15.0f;
            other.gameObject.SetActive(false);
        }
    }

    public void Hurt()
    {
        Life--;
        Texts();
        new WaitForSeconds(1.0f);
    }

    public IEnumerator Timer(float Timer = 60)
    {
        Timer = Seconds;
        while (Seconds > -1 && Life!= 0)
        {
            TextTimer.text = " Seconds left = " + Seconds;
            yield return new WaitForSeconds(1.0f);
            Seconds--;
            if (Seconds == 0)
            {
                GameOver();
            }
        }
    }
}