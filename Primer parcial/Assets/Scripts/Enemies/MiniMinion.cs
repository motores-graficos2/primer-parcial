using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMinion : MonoBehaviour
{
    private int hp;
    private GameObject Player;
    public int Speed;
    void Start()
    {
        hp = 100;
        Player = GameObject.Find("Player");
    }

    private void Update()
    {
        transform.LookAt(Player.transform);
        transform.Translate(Speed * Vector3.forward * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            Da�ado();
        }
    }

    public void Da�ado()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.disappear();
        }
    }

    private void disappear()
    {
        Destroy(gameObject);
    }
}
