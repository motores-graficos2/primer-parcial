using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turrets : MonoBehaviour
{
    private int hp;
    private GameObject Player;
    public int Speed;
    public GameObject Projectile;
    public Camera TCam;

    void Start()
    {
        hp = 100;
        Player = GameObject.Find("Player");
    }

    private void Update()
    {
        transform.LookAt(Player.transform);
        
        /*Ray ray = TCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

        GameObject pro;
        pro = Instantiate(Projectile, ray.origin, transform.rotation);

        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb.AddForce(TCam.transform.forward * 50, ForceMode.Impulse);

        Destroy(pro, 1);*/
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            Da�ado();
        }
    }

    public void Da�ado()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.disappear();
        }
    }

    private void disappear()
    {
        Destroy(gameObject);
    }
}