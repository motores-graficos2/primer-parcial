using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    Vector2 Look;
    Vector2 Smoothness;
    public float Sensitivity = 5.0f;
    public float Softened = 2.0f;
    GameObject Player;

    void Start()
    {
        Player = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        
        md = Vector2.Scale(md, new Vector2(Sensitivity * Softened, Sensitivity * Softened)); 
        
        Smoothness.x = Mathf.Lerp(Smoothness.x, md.x, 1f / Softened);
        Smoothness.y = Mathf.Lerp(Smoothness.y, md.y, 1f / Softened); 
        
        Look += Smoothness; 
        Look.y = Mathf.Clamp(Look.y, -90f, 90f); 
        transform.localRotation = Quaternion.AngleAxis(-Look.y, Vector3.right); 
        Player.transform.localRotation = Quaternion.AngleAxis(Look.x, Player.transform.up);
    }
}
