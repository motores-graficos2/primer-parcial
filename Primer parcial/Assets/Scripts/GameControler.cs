using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControler : MonoBehaviour
{
    public GameObject Player; 
    public GameObject BasicMinion;
    public GameObject Turret;
    public GameObject MiniMinion;
    public GameObject Targets;
    public GameObject Boss;
    public GameObject PowerUp;
    private List<GameObject> EnemyList = new List<GameObject>(); 
    float Timer;

    void Start()
    {
        StartGame();
    }

    void Update()
    {
        if(Timer==0)
        {
            StartGame();
        }

        if(Input.GetKeyDown("r"))
        {
            StartGame();
        }
    }

    void StartGame()
    {
        Player.transform.position = new Vector3(0f, 1f, 0f);

        foreach (GameObject item in EnemyList)
        {
            Destroy(item);
        }

        EnemyList.Add(Instantiate(BasicMinion, new Vector3(45f, 1.5f, 20f), Quaternion.identity));
        EnemyList.Add(Instantiate(BasicMinion, new Vector3(45f, 1.5f, 7f), Quaternion.identity));
        EnemyList.Add(Instantiate(BasicMinion, new Vector3(45f, 1.5f, -7f), Quaternion.identity));
        EnemyList.Add(Instantiate(BasicMinion, new Vector3(45f, 1.5f, -20f), Quaternion.identity));
        EnemyList.Add(Instantiate(Turret, new Vector3(50f, 2f, 25f), Quaternion.identity));
        EnemyList.Add(Instantiate(Turret, new Vector3(50f, 2f, -25f), Quaternion.identity));
        EnemyList.Add(Instantiate(Turret, new Vector3(25f, 2f, 15f), Quaternion.identity));
        EnemyList.Add(Instantiate(Turret, new Vector3(25f, 2f, -15f), Quaternion.identity));
        EnemyList.Add(Instantiate(MiniMinion, new Vector3(55f, 1f, 15f), Quaternion.identity));
        EnemyList.Add(Instantiate(MiniMinion, new Vector3(55f, 1f, -15f), Quaternion.identity));
        /*   EnemyList.Add(Instantiate(Targets, new Vector3(57f, 13f, 20f), Quaternion.identity));
           EnemyList.Add(Instantiate(Targets, new Vector3(57f, 13f, -20f), Quaternion.identity));
           EnemyList.Add(Instantiate(Targets, new Vector3(57f, 5f, 9f), Quaternion.identity));
           EnemyList.Add(Instantiate(Targets, new Vector3(57f, 5f, -9f), Quaternion.identity));*/
        EnemyList.Add(Instantiate(Boss, new Vector3(51f, 4f, 0f), Quaternion.identity));
        EnemyList.Add(Instantiate(PowerUp, new Vector3(50f, 2f, 0f), Quaternion.identity)); 
        StartCoroutine(StartTimer(60));
    }

    public IEnumerator StartTimer(float TimerValue = 60)
    {
        Timer = TimerValue;
        while (Timer > 0)
        {
            Debug.Log(Timer + "seconds left");
            yield return new WaitForSeconds(1.0f);
            Timer--;
        }
    }

}
